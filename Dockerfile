# Build Stage
#
FROM node:16-alpine AS Build

WORKDIR /app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install

COPY . .

RUN yarn build


# Production Stage
#
FROM node:16-alpine

WORKDIR /app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install --production

COPY --from=Build /app/dist ./dist

EXPOSE 3000

CMD ["yarn","start:prod"]

