<h1 align="center">Exercise cookunity</h1>

## Links

- [Repo](https://gitlab.com/yosuer/cookunity-exercise "<project-name> Repo")

- [Live demo](https://cookunity.yosuer.com/api)

## Built With

- Node 16.14.2 & Yarn 1.22.19
- NestJs: [https://nestjs.com](https://nestjs.com/)
- docker-compose: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

## Features

- Track ip [doc](https://cookunity.yosuer.com/api)
- Get statistics [doc](https://cookunity.yosuer.com/api)

## Installation

```bash
$ yarn install
```

## Running the local app

```bash
# copy enviroment variables
$ cp .env.example .env

# update CURRENCY_SERVICE_FIXERIO_KEY from https://api.apilayer.com/fixer 

# up the infrastructure
$ docker-compose up -d

# run app (watch mode)
$ yarn start:dev
```

## Test

```bash
# unit tests
$ yarn test

# test coverage
$ yarn test:cov

# integration test
$ yarn test:e2e:docker
```
