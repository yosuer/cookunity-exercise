import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { CommonModule } from './modules/common/common.module';
import { StatisticsModule } from './modules/statistics/statistics.module';
import { TraceModule } from './modules/traces/trace.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    CommonModule,
    TraceModule,
    StatisticsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
