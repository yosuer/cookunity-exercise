import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse } from '@nestjs/swagger';
import { GetStatisticsUseCase } from '../../modules/statistics/application/get-statistics.usecase';
import { StatisticsResponse } from '../dtos/statistics.response';

@Controller('statistics')
export class StatisticsController {
  constructor(private readonly getStatisticsUseCase: GetStatisticsUseCase) {}

  @Get()
  @ApiOkResponse({
    type: StatisticsResponse,
  })
  async getStatistics(): Promise<StatisticsResponse> {
    const longestDistance = await this.getStatisticsUseCase.longestDistance();
    const mostTraced = await this.getStatisticsUseCase.mostTracedCountry();
    return new StatisticsResponse(longestDistance, mostTraced);
  }
}
