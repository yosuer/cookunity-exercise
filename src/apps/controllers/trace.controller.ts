import { Body, Controller, Post } from '@nestjs/common';
import { ApiOkResponse } from '@nestjs/swagger';
import { SaveTraceUseCase } from '../../modules/traces/application/save-trace.usecase';
import { TraceRequest } from '../dtos/trace.request';
import { TraceResponse } from '../dtos/trace.response';

@Controller('traces')
export class TraceController {
  constructor(private readonly saveTraceUseCase: SaveTraceUseCase) {}

  @Post()
  @ApiOkResponse({
    type: TraceResponse,
  })
  async saveTrace(@Body() traceRequest: TraceRequest): Promise<TraceResponse> {
    const info = await this.saveTraceUseCase.run(traceRequest.ip);
    return new TraceResponse(info);
  }
}
