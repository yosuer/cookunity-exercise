import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { CountryScore } from '../../modules/statistics/domain/country-score.entity';

class StatisticsByCountry {
  constructor(country: string, value: number) {
    this.country = country;
    this.value = value;
  }

  @ApiProperty({
    example: 'United States',
  })
  country: string;

  @ApiProperty({
    example: 0,
  })
  value: number;
}

export class StatisticsResponse {
  constructor(longestDistance: CountryScore, mostTraced: CountryScore) {
    if (longestDistance) {
      this.longestDistance = new StatisticsByCountry(
        longestDistance.code,
        longestDistance.value,
      );
    }
    if (mostTraced) {
      this.mostTraced = new StatisticsByCountry(
        mostTraced.code,
        mostTraced.value,
      );
    }
  }

  @ApiProperty({
    type: StatisticsByCountry,
  })
  @Expose({ name: 'longest_distance' })
  longestDistance: StatisticsByCountry;

  @ApiProperty({
    type: StatisticsByCountry,
  })
  @Expose({ name: 'most_traced' })
  mostTraced: StatisticsByCountry;
}
