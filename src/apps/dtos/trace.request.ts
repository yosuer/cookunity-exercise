import { ApiProperty } from '@nestjs/swagger';
import { IsIP } from 'class-validator';

export class TraceRequest {
  @IsIP(4)
  @ApiProperty({
    required: true,
    description: 'ip',
    example: '167.62.158.169',
  })
  ip: string;
}
