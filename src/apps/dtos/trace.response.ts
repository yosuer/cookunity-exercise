import { ApiProperty } from '@nestjs/swagger';
import { Expose } from 'class-transformer';
import { Trace } from '../../modules/traces/domain/trace.entity';

class Currency {
  constructor(iso: string, symbol: string, conversionRate: number) {
    this.iso = iso;
    this.symbol = symbol;
    this.conversionRate = conversionRate;
  }

  @ApiProperty({
    example: 'ARS',
  })
  iso: string;

  @ApiProperty({
    example: '$',
  })
  symbol: string;

  @ApiProperty({
    example: 134.26915,
    name: 'convertion_rate',
  })
  @Expose({ name: 'convertion_rate' })
  conversionRate: number;
}

export class TraceResponse {
  constructor(trace: Trace) {
    this.ip = trace.ip;
    this.code = trace.country.code;
    this.name = trace.country.name;
    this.lat = trace.location.lat;
    this.lon = trace.location.lon;
    this.currencies = trace.currencies.map(
      ({ iso, symbol, conversionRate }) =>
        new Currency(iso, symbol, conversionRate),
    );
    this.distanceToUsa = trace.distanceToUsa;
  }

  @ApiProperty({
    example: '167.62.158.169',
  })
  ip: string;

  @ApiProperty({
    example: 'Uruguay',
  })
  name: string;

  @ApiProperty({
    example: 'UY',
  })
  code: string;

  @ApiProperty({
    example: -34.0939,
  })
  lat: number;

  @ApiProperty({
    example: -56.2186,
  })
  lon: number;

  @ApiProperty({
    type: [Currency],
  })
  currencies: Currency[];

  @ApiProperty({
    example: 10000,
    name: 'distance_to_usa',
  })
  @Expose({ name: 'distance_to_usa' })
  distanceToUsa: number;
}
