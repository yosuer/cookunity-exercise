import { ConfigService } from '@nestjs/config';
import { nanoid } from 'nanoid';
import { Params } from 'nestjs-pino';

export const loggerConfig = (configService: ConfigService): Params => ({
  pinoHttp: {
    transport:
      configService.get<string>('LOG_PRETTY_ENABLED') === 'true'
        ? {
            target: 'pino-pretty',
            options: {
              colorize: true,
              singleLine: true,
              translateTime: "yyyy-mm-dd'T'HH:MM:ss.l'Z'",
              messageFormat: '{req.id} [{context}] {msg}',
              ignore: 'pid,hostname,context',
            },
          }
        : undefined,
    genReqId: (req) => {
      return req.headers.requestId || nanoid();
    },
    level: configService.get<string>('LOG_LEVEL') || 'info',
    formatters: {
      level: (label) => {
        return { level: label };
      },
    },
    serializers: {
      req(req) {
        req.body = req.raw.body;
        return req;
      },
    },
  },
});
