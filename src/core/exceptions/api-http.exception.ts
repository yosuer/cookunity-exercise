import { HttpException } from '@nestjs/common';

export class ApiHttpException extends HttpException {
  private readonly detail: any;
  private readonly type: string;

  constructor(status: number, message: string, type: string, detail?: any) {
    super(message, status);
    this.type = type;
    this.detail = detail;
  }

  getDetail() {
    return this.detail;
  }

  getType(): string {
    return this.type;
  }
}
