import { ValidationError } from '@nestjs/common';
import { ApiHttpException } from './api-http.exception';

export class InvalidFieldsException extends ApiHttpException {
  constructor(validationErrors: ValidationError[] = []) {
    const detail = {
      validationErrors: validationErrors
        .filter((validationError) => !!validationError.constraints)
        .flatMap((validationError) =>
          Object.values(validationError.constraints),
        ),
    };

    super(400, 'Invalid fields', 'invalid-fields', detail);
  }
}
