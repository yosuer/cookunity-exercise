import { Logger } from '@nestjs/common';
import { AxiosError, AxiosInstance } from 'axios';

interface ErrorInterface {
  response?: {
    data: any;
    headers: any;
    status: string;
  };
  config: any;
  message: string;
}

interface ErrorLogInterface extends ErrorInterface {
  client: string;
}

export abstract class AxiosClient {
  protected readonly logger: Logger = new Logger(AxiosClient.name);

  protected client: AxiosInstance;

  protected errorLogger(error: ErrorInterface, client: string) {
    const errorLog: ErrorLogInterface = {
      client,
      config: error.config,
      message: error.message,
    };

    if (error.response) {
      const { response } = error;

      errorLog.response = {
        data: response.data,
        headers: response.headers,
        status: response.status,
      };
    }

    this.logger.error(errorLog);

    return Promise.reject(error);
  }

  protected loadClientInterceptors(instance: AxiosInstance, name: string) {
    instance.interceptors.request.use((request) => {
      this.logger.debug(`request to ${name}: ${JSON.stringify(request)}`);
      return request;
    });
    instance.interceptors.response.use(
      ({ config, request, ...response }) => {
        this.logger.debug(`response from ${name}: ${JSON.stringify(response)}`);
        return response;
      },
      (error) => this.errorLogger(error, name),
    );
  }

  protected buildDetailFromAxiosError(err: AxiosError) {
    const { code, config, message } = err;
    const detail: any = {
      code,
      url: `${config.baseURL}${config.url}`,
      data: message,
    };
    if (err.response) {
      const { status, statusText, data } = err.response;
      detail.code = `${status}(${statusText})`;
      detail.data = data;
    }
    return detail;
  }
}
