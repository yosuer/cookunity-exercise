import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
} from '@nestjs/common';
import { Response } from 'express';
import { BaseException } from '../../modules/traces/exceptions/base.exception';
import { ApiHttpException } from '../exceptions/api-http.exception';
import { ApiErrorInterface } from '../interfaces/api-error.interface';

@Catch()
export class FallbackExceptionsFilter implements ExceptionFilter {
  catch(err: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const status =
      err?.response?.statusCode || HttpStatus.INTERNAL_SERVER_ERROR;
    const responseBody: ApiErrorInterface = {
      message: err?.response?.error || 'Internal Server Error',
      detail: err?.response?.message,
    };
    response.status(status).json(responseBody);
  }
}

@Catch(BaseException)
export class BaseExceptionsFilter implements ExceptionFilter {
  catch(exception: BaseException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const status = HttpStatus.INTERNAL_SERVER_ERROR;
    const responseBody: ApiErrorInterface = {
      message: exception.message,
      detail: exception.getDetail(),
      type: exception.getType(),
    };
    response.status(status).json(responseBody);
  }
}

@Catch(ApiHttpException)
export class ApiHttpExceptionFilter implements ExceptionFilter {
  catch(exception: ApiHttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const status = exception.getStatus();
    const responseJson: ApiErrorInterface = {
      message: exception.message,
      detail: exception.getDetail(),
      type: exception.getType(),
    };
    response.status(status).json(responseJson);
  }
}
