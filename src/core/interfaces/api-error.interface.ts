export interface ApiErrorInterface {
  type?: string;
  detail?: any;
  message: string;
}
