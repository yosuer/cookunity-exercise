import {
  ClassSerializerInterceptor,
  INestApplication,
  ValidationError,
  ValidationPipe,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Logger } from 'nestjs-pino';
import { InvalidFieldsException } from '../exceptions/invalid-fields.exception';
import {
  ApiHttpExceptionFilter,
  BaseExceptionsFilter,
  FallbackExceptionsFilter,
} from '../interceptors/http-exception.filter';

const appSetup = async (app: INestApplication) => {
  app.useLogger(app.get(Logger));

  app.getHttpAdapter().getInstance().disable('x-powered-by');

  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (validationErrors: ValidationError[] = []) => {
        return new InvalidFieldsException(validationErrors);
      },
      transform: true,
    }),
  );
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));

  app.useGlobalFilters(new FallbackExceptionsFilter());
  app.useGlobalFilters(new BaseExceptionsFilter());
  app.useGlobalFilters(new ApiHttpExceptionFilter());
};

export default appSetup;
