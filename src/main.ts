import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import appSetup from './core/setup/app.setup';
import swaggerSetup from './core/setup/swagger.setup';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true });

  appSetup(app);
  swaggerSetup(app);

  await app.listen(3000);
}
bootstrap();
