import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { RedisModule } from '@liaoliaots/nestjs-redis';
import { LoggerModule } from 'nestjs-pino';
import { TRACE_REPOSITORY } from '../common/domain/trace.repository';
import { TraceRepositoryRedis } from '../common/infrastructure/trace.repository.redis';
import { redisConfig } from '../../core/configs/redis.config';
import { loggerConfig } from '../../core/configs/logger.config';

const TraceRepositoryProvider = {
  provide: TRACE_REPOSITORY,
  useClass: TraceRepositoryRedis,
};

@Module({
  imports: [
    RedisModule.forRootAsync(
      {
        inject: [ConfigService],
        useFactory: redisConfig,
      },
      false,
    ),
    LoggerModule.forRootAsync({
      inject: [ConfigService],
      useFactory: loggerConfig,
    }),
  ],
  providers: [TraceRepositoryProvider],
  exports: [TraceRepositoryProvider],
})
export class CommonModule {}
