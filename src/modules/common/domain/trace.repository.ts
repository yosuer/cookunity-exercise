import { CountryScore } from '../../statistics/domain/country-score.entity';
import { Trace } from '../../traces/domain/trace.entity';

export const TRACE_REPOSITORY = 'TRACE_REPOSITORY';

export interface TraceRepository {
  saveMostTracedCountry(countryName: string): Promise<void>;

  saveLongestCountry(trace: Trace): Promise<void>;

  getMostTracedCountry(): Promise<CountryScore | null>;

  getLongestCountry(): Promise<CountryScore | null>;
}
