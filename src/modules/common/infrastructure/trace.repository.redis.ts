import { Injectable } from '@nestjs/common';
import { InjectRedis } from '@liaoliaots/nestjs-redis';
import Redis from 'ioredis';
import { CountryScore } from '../../statistics/domain/country-score.entity';
import { Trace } from '../../traces/domain/trace.entity';
import { TraceRepository } from '../domain/trace.repository';

@Injectable()
export class TraceRepositoryRedis implements TraceRepository {
  private readonly SET_COUNTRY_COUNTER = 'countries:counter';
  private readonly SET_COUNTRY_LONGEST = 'countries:longest';

  constructor(@InjectRedis() private readonly redis: Redis) {}

  async saveMostTracedCountry(countryName: string): Promise<void> {
    await this.redis.zincrby(this.SET_COUNTRY_COUNTER, 1, countryName);
  }

  async saveLongestCountry(trace: Trace): Promise<void> {
    const { country, distanceToUsa } = trace;
    await this.redis
      .multi()
      .zadd(
        this.SET_COUNTRY_LONGEST,
        distanceToUsa,
        `${country.name}-${trace.ip}`,
      )
      .zremrangebyrank(this.SET_COUNTRY_LONGEST, -2, -2)
      .exec();
  }

  async getMostTracedCountry(): Promise<CountryScore> {
    const [name, count] = await this.redis.zrevrange(
      this.SET_COUNTRY_COUNTER,
      0,
      0,
      'WITHSCORES',
    );
    if (!name) return null;
    return new CountryScore(name, +count);
  }

  async getLongestCountry(): Promise<CountryScore> {
    const [nameWithIp, count] = await this.redis.zrevrange(
      this.SET_COUNTRY_LONGEST,
      0,
      0,
      'WITHSCORES',
    );
    if (!nameWithIp) return null;
    const [name] = nameWithIp.split('-');
    return new CountryScore(name, +count);
  }
}
