import { Inject, Injectable, Logger } from '@nestjs/common';
import { CountryScore } from '../domain/country-score.entity';
import {
  TraceRepository,
  TRACE_REPOSITORY,
} from '../../common/domain/trace.repository';

@Injectable()
export class GetStatisticsUseCase {
  private readonly logger: Logger = new Logger(GetStatisticsUseCase.name);

  constructor(
    @Inject(TRACE_REPOSITORY)
    private traceRepository: TraceRepository,
  ) {}

  mostTracedCountry(): Promise<CountryScore> {
    this.logger.log(`Getting most traced country`);
    return this.traceRepository.getMostTracedCountry();
  }

  longestDistance(): Promise<CountryScore> {
    this.logger.log(`Getting longest distance country`);
    return this.traceRepository.getLongestCountry();
  }
}
