export class CountryScore {
  readonly code: string;
  readonly value: number;

  constructor(code: string, value: number) {
    this.code = code;
    this.value = value;
  }
}
