import { Module } from '@nestjs/common';
import { CommonModule } from '../common/common.module';
import { StatisticsController } from '../../apps/controllers/statistics.controller';
import { GetStatisticsUseCase } from '../statistics/application/get-statistics.usecase';

@Module({
  controllers: [StatisticsController],
  providers: [GetStatisticsUseCase],
  imports: [CommonModule],
})
export class StatisticsModule {}
