import { Inject, Injectable, Logger } from '@nestjs/common';
import { Trace } from '../domain/trace.entity';
import { CurrencyService, CURRENCY_SERVICE } from '../domain/currency.service';
import {
  GeolocationService,
  GEOLOCATION_SERVICE,
} from '../domain/geo-location.service';
import {
  TraceRepository,
  TRACE_REPOSITORY,
} from '../../common/domain/trace.repository';

@Injectable()
export class SaveTraceUseCase {
  private readonly logger: Logger = new Logger(SaveTraceUseCase.name);

  constructor(
    @Inject(GEOLOCATION_SERVICE)
    private geoLocationService: GeolocationService,
    @Inject(CURRENCY_SERVICE)
    private currencyService: CurrencyService,
    @Inject(TRACE_REPOSITORY)
    private traceRepository: TraceRepository,
  ) {}

  async run(ip: string): Promise<Trace> {
    this.logger.log(`Tracing ${ip}`);
    const trace = new Trace(ip);
    const { country, location, currency } =
      await this.geoLocationService.infoByIp(ip);
    trace.country = country;
    trace.location = location;

    trace.distanceToUsa = this.geoLocationService.getDistanceToUsa(location);

    const currencies = await this.currencyService.getRateFromBase([currency]);

    trace.currencies = currencies;

    await this.traceRepository.saveMostTracedCountry(trace.country.name);

    await this.traceRepository.saveLongestCountry(trace);

    return trace;
  }
}
