export class Currency {
  readonly iso: string;
  symbol: string;
  conversionRate: number;

  constructor(iso: string) {
    this.iso = iso;
  }
}
