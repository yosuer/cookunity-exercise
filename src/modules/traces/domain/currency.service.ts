import { Currency } from './currency.entity';

export const CURRENCY_SERVICE = 'CURRENCY_SERVICE';

export interface CurrencyService {
  getRateFromBase(currencies: string[]): Promise<Currency[]>;
}
