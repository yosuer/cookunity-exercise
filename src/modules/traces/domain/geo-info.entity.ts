import { Country } from './country.entity';
import { Location } from './location.entity';

export class GeoInfo {
  country: Country;
  location: Location;
  currency: string;

  constructor(country: Country, location: Location, currency: string) {
    this.country = country;
    this.location = location;
    this.currency = currency;
  }
}
