import { GeoInfo } from './geo-info.entity';
import { Location } from './location.entity';

export const GEOLOCATION_SERVICE = 'GEOLOCATION_SERVICE';

export interface GeolocationService {
  infoByIp(ip: string): Promise<GeoInfo>;

  getDistanceToUsa(from: Location): number;
}
