import { Currency } from './currency.entity';

class Country {
  code: string;
  name: string;
}

class Location {
  lat: number;
  lon: number;
}

export class Trace {
  readonly ip: string;
  country: Country;
  location: Location;
  currencies: Currency[];
  distanceToUsa: number;

  constructor(ip: string) {
    this.ip = ip;
  }
}
