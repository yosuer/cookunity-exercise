export abstract class BaseException extends Error {
  private readonly detail: any;
  private readonly type: string;

  constructor(message: string, type: string, detail?: any) {
    super(message);
    this.type = type;
    this.detail = detail;
  }

  getDetail() {
    return this.detail;
  }

  getType(): string {
    return this.type;
  }
}
