import { BaseException } from './base.exception';

export class CurrencyServiceError extends BaseException {
  constructor(message: string, detail?: any) {
    super(message, 'CurrencyServiceError', detail);
  }
}
