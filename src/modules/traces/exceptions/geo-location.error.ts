import { BaseException } from './base.exception';

export class GeoLocationError extends BaseException {
  constructor(message: string, detail?: any) {
    super(message, 'GeolocationError', detail);
  }
}
