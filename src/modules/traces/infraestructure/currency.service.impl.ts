import axios from 'axios';
import { ConfigService } from '@nestjs/config';
import { Injectable, Logger } from '@nestjs/common';
import symbols from './symbols';
import { Currency } from '../domain/currency.entity';
import { CurrencyService } from '../domain/currency.service';
import { CurrencyServiceError } from '../exceptions/currency-service.error';
import { AxiosClient } from '../../../core/http-clients/axios-client.impl';

@Injectable()
export class CurrencyServiceImpl
  extends AxiosClient
  implements CurrencyService
{
  protected readonly logger: Logger = new Logger(CurrencyServiceImpl.name);

  constructor(private configService: ConfigService) {
    super();
    this.client = axios.create({
      baseURL: this.configService.get('CURRENCY_SERVICE_FIXERIO_URL'),
      headers: {
        apikey: this.configService.get('CURRENCY_SERVICE_FIXERIO_KEY'),
      },
      timeout: this.configService.get('CURRENCY_SERVICE_FIXERIO_TIMEOUT'),
    });
    this.loadClientInterceptors(this.client, 'currency-service');
  }

  async getRateFromBase(currencies: string[]): Promise<Currency[]> {
    const base = this.configService.get('CURRENCY_SERVICE_BASE');
    this.logger.debug(
      `Getting rate from base ${base} for currencies ${JSON.stringify(
        currencies,
      )}`,
    );
    try {
      const { data } = await this.client.get(
        `/latest?base=${base}&symbols=${currencies.join(',')}`,
      );
      if (data.success) {
        return Object.keys(data.rates).map((code) => {
          const currency = new Currency(code);
          currency.symbol = symbols[code];
          currency.conversionRate = data.rates[code];
          return currency;
        });
      }
      throw new CurrencyServiceError(data.error?.info, data.error);
    } catch (err) {
      this.logger.error(err);
      if (err.isAxiosError) {
        const detail = this.buildDetailFromAxiosError(err);
        throw new CurrencyServiceError(
          'An unexpected error has occurred obtaining the rates',
          detail,
        );
      }
      throw err;
    }
  }
}
