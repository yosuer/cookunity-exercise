import axios from 'axios';
import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { getDistance } from 'geolib';
import { GeoInfo } from '../domain/geo-info.entity';
import { GeolocationService } from '../domain/geo-location.service';
import { Country } from '../domain/country.entity';
import { Location } from '../domain/location.entity';
import { GeoLocationError } from '../exceptions/geo-location.error';
import { AxiosClient } from '../../../core/http-clients/axios-client.impl';

const fields = 'status,message,country,countryCode,lat,lon,currency';
const METERS_BY_KILOMETER = 1000;
@Injectable()
export class GeoLocationServiceImpl
  extends AxiosClient
  implements GeolocationService
{
  protected readonly logger: Logger = new Logger(GeoLocationServiceImpl.name);

  constructor(private configService: ConfigService) {
    super();
    this.client = axios.create({
      baseURL: this.configService.get('GEOLOCATION_SERVICE_IP_URL'),
      timeout: this.configService.get('GEOLOCATION_SERVICE_IP_TIMEOUT'),
    });
    this.loadClientInterceptors(this.client, 'geolocation-service');
  }

  async infoByIp(ip: string): Promise<GeoInfo> {
    this.logger.debug(`Getting infoByIp ${ip}`);
    try {
      const { data } = await this.client.get(`/${ip}?fields=${fields}`);
      if (data.status === 'success') {
        const { country, countryCode, lat, lon, currency } = data;
        return new GeoInfo(
          new Country(countryCode, country),
          new Location(lat, lon),
          currency,
        );
      }
      throw new GeoLocationError(data);
    } catch (err) {
      this.logger.error(err);
      if (err.isAxiosError) {
        const detail = this.buildDetailFromAxiosError(err);
        throw new GeoLocationError(
          'An unexpected error has occurred obtaining the rates',
          detail,
        );
      }
      throw err;
    }
  }

  getDistanceToUsa(from: Location): number {
    this.logger.debug(`Getting distance to Usa from ${JSON.stringify(from)}`);
    const usaLatitude = this.configService.get('USA_LATITUDE');
    const usaLongitude = this.configService.get('USA_LONGITUDE');
    const distanceInMeters = getDistance(
      { latitude: from.lat, longitude: from.lon },
      { latitude: usaLatitude, longitude: usaLongitude },
    );
    return distanceInMeters / METERS_BY_KILOMETER;
  }
}
