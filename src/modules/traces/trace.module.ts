import { Module } from '@nestjs/common';
import { TraceController } from '../../apps/controllers/trace.controller';
import { CommonModule } from '../common/common.module';
import { SaveTraceUseCase } from './application/save-trace.usecase';
import { CURRENCY_SERVICE } from './domain/currency.service';
import { GEOLOCATION_SERVICE } from './domain/geo-location.service';
import { CurrencyServiceImpl } from './infraestructure/currency.service.impl';
import { GeoLocationServiceImpl } from './infraestructure/geo-location.service.impl';

@Module({
  controllers: [TraceController],
  providers: [
    { provide: GEOLOCATION_SERVICE, useClass: GeoLocationServiceImpl },
    { provide: CURRENCY_SERVICE, useClass: CurrencyServiceImpl },
    SaveTraceUseCase,
  ],
  imports: [CommonModule],
})
export class TraceModule {}
