import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import {
  getRedisToken,
  DEFAULT_REDIS_NAMESPACE,
} from '@liaoliaots/nestjs-redis';
import Redis from 'ioredis';
import * as request from 'supertest';
import * as path from 'path';
import appConfig from '../../src/core/setup/app.setup';
import { StatisticsModule } from '../../src/modules/statistics/statistics.module';
import { CommonModule } from '../../src/modules/common/common.module';

const SET_COUNTRY_COUNTER = 'countries:counter';
const SET_COUNTRY_LONGEST = 'countries:longest';

describe('StatisticsController (e2e)', () => {
  let app: INestApplication;
  let redisClient: Redis;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: path.join(process.cwd(), '.env.test'),
          isGlobal: true,
        }),
        CommonModule,
        StatisticsModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    appConfig(app);

    redisClient = moduleFixture.get(getRedisToken(DEFAULT_REDIS_NAMESPACE));
    await redisClient.del(SET_COUNTRY_COUNTER);
    await redisClient.del(SET_COUNTRY_LONGEST);

    await app.init();
  });

  it('/statistics (GET), with no records, returns empty', () => {
    return request(app.getHttpServer())
      .get('/statistics')
      .expect(200)
      .expect({});
  });

  it('/statistics (GET), with records, returns data', async () => {
    await redisClient
      .multi()
      .zincrby(SET_COUNTRY_COUNTER, 1, 'AR')
      .zincrby(SET_COUNTRY_COUNTER, 1, 'AR')
      .zincrby(SET_COUNTRY_COUNTER, 1, 'NZ')
      .zadd(SET_COUNTRY_LONGEST, 10000, 'NZ-1.1.1.1')
      .zadd(SET_COUNTRY_LONGEST, 5000, 'AR-1.1.1.2')
      .exec();

    return request(app.getHttpServer())
      .get('/statistics')
      .expect(200)
      .expect({
        longest_distance: { country: 'NZ', value: 10000 },
        most_traced: { country: 'AR', value: 2 },
      });
  });

  afterEach(async () => {
    await app.close();
  });
});
