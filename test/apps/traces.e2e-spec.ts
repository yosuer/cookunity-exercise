import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {
  getRedisToken,
  DEFAULT_REDIS_NAMESPACE,
} from '@liaoliaots/nestjs-redis';
import { getDistance } from 'geolib';
import Redis from 'ioredis';
import * as request from 'supertest';
import * as path from 'path';
import appConfig from '../../src/core/setup/app.setup';
import { CommonModule } from '../../src/modules/common/common.module';
import { TraceModule } from '../../src/modules/traces/trace.module';

const SET_COUNTRY_COUNTER = 'countries:counter';
const SET_COUNTRY_LONGEST = 'countries:longest';

describe('TracesController (e2e)', () => {
  let app: INestApplication;
  let redisClient: Redis;
  const axiosMock = new MockAdapter(axios);

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: path.join(process.cwd(), '.env.test'),
          isGlobal: true,
        }),
        CommonModule,
        TraceModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication();

    appConfig(app);

    redisClient = moduleFixture.get(getRedisToken(DEFAULT_REDIS_NAMESPACE));
    await redisClient.del(SET_COUNTRY_COUNTER);
    await redisClient.del(SET_COUNTRY_LONGEST);

    axiosMock.reset();
    await app.init();
  });

  it('/traces (POST), with no ip, returns BadRequest', () => {
    return request(app.getHttpServer())
      .post('/traces')
      .send()
      .expect(400)
      .expect({
        message: 'Invalid fields',
        detail: { validationErrors: ['ip must be an ip address'] },
        type: 'invalid-fields',
      });
  });

  it('/traces (POST), with ok calls, returns info', () => {
    const ip = '110.110.110.100';
    const country = { code: 'AR', name: 'Argentina' };
    const location = { lat: -35.8638, lon: -57.8967 };
    const currency = 'ARS';
    const expectedDistance =
      getDistance(
        { latitude: location.lat, longitude: location.lon },
        {
          latitude: process.env.USA_LATITUDE,
          longitude: process.env.USA_LONGITUDE,
        },
      ) / 1000;

    axiosMock
      .onGet(new RegExp(`${process.env.GEOLOCATION_SERVICE_IP_URL}/${ip}`))
      .reply(200, {
        status: 'success',
        country: country.name,
        countryCode: country.code,
        lat: location.lat,
        lon: location.lon,
        currency,
      });

    axiosMock
      .onGet(new RegExp(`${process.env.CURRENCY_SERVICE_FIXERIO_URL}/latest`))
      .reply(200, { success: true, rates: { USD: 1, CAD: 2 } });

    return request(app.getHttpServer())
      .post('/traces')
      .send({ ip })
      .expect(201)
      .expect({
        ip,
        code: country.code,
        name: country.name,
        lat: location.lat,
        lon: location.lon,
        currencies: [
          {
            iso: 'USD',
            symbol: '$',
            convertion_rate: 1,
          },
          {
            iso: 'CAD',
            symbol: '$',
            convertion_rate: 2,
          },
        ],
        distance_to_usa: expectedDistance,
      });
  });

  afterEach(async () => {
    await app.close();
  });
});
