import { Currency } from '../../../src/modules/traces/domain/currency.entity';
import { CurrencyService } from '../../../src/modules/traces/domain/currency.service';

export class CurrencyServiceMock implements CurrencyService {
  private mockgetRate = jest.fn();

  getRateFromBase(currencies: string[]): Promise<Currency[]> {
    return this.mockgetRate(currencies);
  }

  returnGetRate(mockValue: Currency[]) {
    this.mockgetRate.mockResolvedValueOnce(mockValue);
  }
}
