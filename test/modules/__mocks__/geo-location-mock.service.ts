import { GeoInfo } from 'src/modules/traces/domain/geo-info.entity';
import { Location } from 'src/modules/traces/domain/location.entity';
import { GeolocationService } from '../../../src/modules/traces/domain/geo-location.service';

export class GeolocationServiceMock implements GeolocationService {
  private infoByIpMock = jest.fn();
  private getDistanceMock = jest.fn();

  infoByIp(ip: string): Promise<GeoInfo> {
    return this.infoByIpMock(ip);
  }

  getDistanceToUsa(from: Location): number {
    return this.getDistanceMock(from);
  }

  returnInfoByIp(geoInfoMock: GeoInfo) {
    this.infoByIpMock.mockResolvedValueOnce(geoInfoMock);
  }

  returnGetDistance(distance: number) {
    this.getDistanceMock.mockReturnValueOnce(distance);
  }

  assertGetDistanceHasBeenCalledWith(from: Location) {
    expect(this.getDistanceMock).toHaveBeenCalledWith(from);
  }
}
