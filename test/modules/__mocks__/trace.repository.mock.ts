import { CountryScore } from '../../../src/modules/statistics/domain/country-score.entity';
import { Trace } from '../../../src/modules/traces/domain/trace.entity';
import { TraceRepository } from '../../../src/modules/common/domain/trace.repository';

export class TraceRepositoryMock implements TraceRepository {
  private mockSaveTracedCountry = jest.fn();
  private mockSaveLongestCountry = jest.fn();
  private mockGetLongestCountry = jest.fn();
  private mockMostTracedCountry = jest.fn();

  saveMostTracedCountry(countryName: string): Promise<void> {
    return this.mockSaveTracedCountry(countryName);
  }

  saveLongestCountry(trace: Trace): Promise<void> {
    return this.mockSaveLongestCountry(trace);
  }

  getMostTracedCountry(): Promise<CountryScore> {
    return this.mockMostTracedCountry();
  }

  getLongestCountry(): Promise<CountryScore> {
    return this.mockGetLongestCountry();
  }

  returnMostTracedCountry(countryScore: CountryScore) {
    this.mockMostTracedCountry.mockResolvedValueOnce(countryScore);
  }

  assertMostTracedCountryHasBeenCalled() {
    expect(this.mockMostTracedCountry).toHaveBeenCalledTimes(1);
  }

  returnLongestCountry(countryScore: CountryScore) {
    this.mockGetLongestCountry.mockResolvedValueOnce(countryScore);
  }

  assertLongestCountryHasBeenCalled() {
    expect(this.mockGetLongestCountry).toHaveBeenCalledTimes(1);
  }

  assertSaveMostTracedHasBeenCalledWith(countryName: string) {
    const mock = this.mockSaveTracedCountry.mock;
    const lastSavedMostTraced = mock.calls[mock.calls.length - 1][0];
    expect(lastSavedMostTraced).toEqual(countryName);
  }

  assertSaveLongestHasBeenCalledWith(trace: Trace) {
    const mock = this.mockSaveLongestCountry.mock;
    const lastSavedLongest = mock.calls[mock.calls.length - 1][0];
    expect(lastSavedLongest).toBeInstanceOf(Trace);
    expect(lastSavedLongest).toEqual(trace);
  }
}
