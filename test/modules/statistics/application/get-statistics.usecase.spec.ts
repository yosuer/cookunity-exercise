import { TraceRepositoryMock } from '../../__mocks__/trace.repository.mock';
import { GetStatisticsUseCase } from '../../../../src/modules/statistics/application/get-statistics.usecase';
import { CountryScoreStub } from '../domain/country-score.stub';

describe('GetStatisticsUseCase', () => {
  let getStatisticsUseCase: GetStatisticsUseCase;
  let traceRepositoryMock: TraceRepositoryMock;

  beforeEach(async () => {
    traceRepositoryMock = new TraceRepositoryMock();
    getStatisticsUseCase = new GetStatisticsUseCase(traceRepositoryMock);
  });

  it('longestDistance, should call repository', async () => {
    const countryScore = CountryScoreStub.random();
    traceRepositoryMock.returnLongestCountry(countryScore);

    const result = await getStatisticsUseCase.longestDistance();

    traceRepositoryMock.assertLongestCountryHasBeenCalled();
    expect(result).toEqual(countryScore);
  });

  it('mostTracedCountry, should call repository', async () => {
    const countryScore = CountryScoreStub.random();
    traceRepositoryMock.returnMostTracedCountry(countryScore);

    const result = await getStatisticsUseCase.mostTracedCountry();

    traceRepositoryMock.assertMostTracedCountryHasBeenCalled();
    expect(result).toEqual(countryScore);
  });
});
