import { faker } from '@faker-js/faker';
import { CountryScore } from '../../../../src/modules/statistics/domain/country-score.entity';

export class CountryScoreStub {
  static random(): CountryScore {
    return new CountryScore(
      faker.address.countryCode(),
      faker.datatype.number(),
    );
  }
}
