import { faker } from '@faker-js/faker';
import { SaveTraceUseCase } from '../../../../src/modules/traces/application/save-trace.usecase';
import { CurrencyStub } from '../domain/currency.stub';
import { GeoInfoStub } from '../domain/geo-info.stub';
import { CurrencyServiceMock } from '../../__mocks__/currency-mock.service';
import { GeolocationServiceMock } from '../../__mocks__/geo-location-mock.service';
import { TraceRepositoryMock } from '../../__mocks__/trace.repository.mock';

describe('SaveTraceUseCase', () => {
  let saveTraceUseCase: SaveTraceUseCase;
  let currencyServiceMock: CurrencyServiceMock;
  let geolocationServiceMock: GeolocationServiceMock;
  let traceRepositoryMock: TraceRepositoryMock;

  beforeEach(async () => {
    currencyServiceMock = new CurrencyServiceMock();
    geolocationServiceMock = new GeolocationServiceMock();
    traceRepositoryMock = new TraceRepositoryMock();
    saveTraceUseCase = new SaveTraceUseCase(
      geolocationServiceMock,
      currencyServiceMock,
      traceRepositoryMock,
    );
  });

  it('with valid ip, return info', async () => {
    const ip = faker.internet.ip();

    const geoInfo = GeoInfoStub.random();
    geolocationServiceMock.returnInfoByIp(geoInfo);
    const currencyInfo = CurrencyStub.random();
    currencyServiceMock.returnGetRate([currencyInfo]);

    const result = await saveTraceUseCase.run(ip);

    expect(result.ip).toBe(ip);
    expect(result.country.code).toBe(geoInfo.country.code);
    expect(result.country.name).toBe(geoInfo.country.name);
    expect(result.location.lat).toBe(geoInfo.location.lat);
    expect(result.location.lon).toBe(geoInfo.location.lon);
    expect(result.currencies).toEqual([currencyInfo]);
    geolocationServiceMock.assertGetDistanceHasBeenCalledWith(geoInfo.location);
    traceRepositoryMock.assertSaveMostTracedHasBeenCalledWith(
      result.country.name,
    );
    traceRepositoryMock.assertSaveLongestHasBeenCalledWith(result);
  });
});
