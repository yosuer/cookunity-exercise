import { faker } from '@faker-js/faker';
import symbols from '../../../../src/modules/traces/infraestructure/symbols';
import { Currency } from '../../../../src/modules/traces/domain/currency.entity';

export class CurrencyStub {
  static random(): Currency {
    const currency = new Currency(faker.finance.currencyCode());
    currency.symbol = symbols[currency.iso];
    currency.conversionRate = parseFloat(faker.finance.amount(0, 1000, 6));
    return currency;
  }
}
