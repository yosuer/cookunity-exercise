import { faker } from '@faker-js/faker';
import { Location } from '../../../../src/modules/traces/domain/location.entity';
import { Country } from '../../../../src/modules/traces/domain/country.entity';
import { GeoInfo } from '../../../../src/modules/traces/domain/geo-info.entity';

export class GeoInfoStub {
  static random(): GeoInfo {
    const country = new Country(
      faker.address.country(),
      faker.address.countryCode(),
    );
    const location = new Location(
      parseFloat(faker.address.latitude()),
      parseFloat(faker.address.longitude()),
    );
    const currency = faker.finance.currencyCode();
    return new GeoInfo(country, location, currency);
  }
}
